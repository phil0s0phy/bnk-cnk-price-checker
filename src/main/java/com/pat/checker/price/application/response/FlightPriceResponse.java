package com.pat.checker.price.application.response;

import com.pat.checker.price.domain.Flight;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class FlightPriceResponse {
	private Flight flight;
}
