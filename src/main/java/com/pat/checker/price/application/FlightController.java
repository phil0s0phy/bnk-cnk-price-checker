package com.pat.checker.price.application;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pat.checker.price.application.response.FlightPriceResponse;
import com.pat.checker.price.domain.Flight;
import com.pat.checker.price.domain.service.FlightService;

@RestController
@RequestMapping("/flight")
public class FlightController {
	private final FlightService flightService;

	@Autowired
	public FlightController(FlightService flightService) {
		this.flightService = flightService;
	}

	@GetMapping(value = "/bkk/cnx/{departureDate}/cheapest", produces = MediaType.APPLICATION_JSON_VALUE)
	FlightPriceResponse getCheapestBnkCnxPrice(
			@PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate departureDate) {
		Flight flight = flightService.getCheapestBnkCnxPrice(departureDate);

		return new FlightPriceResponse(flight);
	}
}
