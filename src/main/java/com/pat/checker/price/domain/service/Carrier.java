package com.pat.checker.price.domain.service;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Carrier {
	@SerializedName("CarrierId")
	private int carrierId;

	@SerializedName("Name")
	private String name;
}
