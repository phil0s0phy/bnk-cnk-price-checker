package com.pat.checker.price.domain.service;

import java.util.List;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SkyScannerEntity {
	@SerializedName("Quotes")
	private List<Quote> quotes;

	@SerializedName("Carriers")
	private List<Carrier> carriers;
}
