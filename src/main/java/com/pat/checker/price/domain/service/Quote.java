package com.pat.checker.price.domain.service;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Quote {
	@SerializedName("MinPrice")
	private int minPrice;

	@SerializedName("OutboundLeg")
	private OutboundLeg outboundLeg;
}
