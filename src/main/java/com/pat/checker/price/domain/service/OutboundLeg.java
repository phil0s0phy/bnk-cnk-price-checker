package com.pat.checker.price.domain.service;

import java.util.List;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class OutboundLeg {
	@SerializedName("CarrierIds")
	private List<Integer> carrierIds;
}
