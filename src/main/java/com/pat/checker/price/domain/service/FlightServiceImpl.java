package com.pat.checker.price.domain.service;

import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pat.checker.price.domain.CarrierPrice;
import com.pat.checker.price.domain.Flight;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import okhttp3.OkHttpClient;
import okhttp3.Request;

@RequiredArgsConstructor
@Log4j2
public class FlightServiceImpl implements FlightService {

	private String skyScannerEndpoint;

	private String rapidapiKey;

	private String rapidapiHost;

	private Gson gson = new GsonBuilder().create();

	@Override
	public Flight getCheapestBnkCnxPrice(LocalDate departureDate) {
		SkyScannerEntity rawData = callSkyScannerService(departureDate);
		Map<Integer, String> carrierIdNameMap = extractCarrierName(rawData);
		return extractCarrierPrice("Bangkok (Suvarnabhumi)", "Chiang Mai", departureDate, rawData, carrierIdNameMap);
	}

	Flight extractCarrierPrice(String origin, String destination, LocalDate departureDate,
			SkyScannerEntity skyScannerEntity, Map<Integer, String> carrierIdNameMap) {
		Flight flight = new Flight("Bangkok (Suvarnabhumi)", "Chiang Mai", departureDate);

		if (isNotEmpty(skyScannerEntity.getQuotes())) {
			skyScannerEntity.getQuotes().forEach(quote -> {
				if (quote.getOutboundLeg() != null && isNotEmpty(quote.getOutboundLeg().getCarrierIds())) {
					quote.getOutboundLeg().getCarrierIds().forEach(carrierId -> {
						flight.addCarrierPrice(new CarrierPrice(
								carrierIdNameMap.containsKey(carrierId) ? carrierIdNameMap.get(carrierId) : "Unknown",
								new BigDecimal(quote.getMinPrice())));
					});
				} else {
					flight.addCarrierPrice(new CarrierPrice("Unknown", new BigDecimal(quote.getMinPrice())));
				}
			});
		}

		return flight;
	}

	Map<Integer, String> extractCarrierName(SkyScannerEntity skyScannerEntity) {
		Map<Integer, String> carrierIdNameMap = new HashMap<>();
		if (isNotEmpty(skyScannerEntity.getCarriers())) {
			skyScannerEntity.getCarriers().forEach(carrier -> {
				carrierIdNameMap.put(carrier.getCarrierId(), carrier.getName());
			});
		}

		return carrierIdNameMap;
	}

	SkyScannerEntity callSkyScannerService(LocalDate departureDate) {
		OkHttpClient client = new OkHttpClient();

		Request request = new Request.Builder().url(skyScannerEndpoint + departureDate).get()
				.addHeader("x-rapidapi-key", rapidapiKey).addHeader("x-rapidapi-host", rapidapiHost).build();

		try {
			return gson.fromJson(client.newCall(request).execute().body().string(), SkyScannerEntity.class);
		} catch (IOException e) {
			log.error("Error occurred", e);
		}

		return new SkyScannerEntity();
	}

	@Value("${sky.scanner.endpoint}")
	public void setSkyScannerEndpoint(String skyScannerEndpoint) {
		this.skyScannerEndpoint = skyScannerEndpoint;
	}

	@Value("${rapidapi.key}")
	public void setRapidapiKey(String rapidapiKey) {
		this.rapidapiKey = rapidapiKey;
	}

	@Value("${rapidapi.host}")
	public void setRapidapiHost(String rapidapiHost) {
		this.rapidapiHost = rapidapiHost;
	}
}
