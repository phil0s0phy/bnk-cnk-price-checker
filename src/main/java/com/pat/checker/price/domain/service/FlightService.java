package com.pat.checker.price.domain.service;

import java.time.LocalDate;

import com.pat.checker.price.domain.Flight;

public interface FlightService {

	Flight getCheapestBnkCnxPrice(LocalDate departureDate);

}
