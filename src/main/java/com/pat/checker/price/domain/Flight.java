package com.pat.checker.price.domain;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@RequiredArgsConstructor
@Getter
public class Flight {
	@NonNull
	private String origin;

	@NonNull
	private String destination;

	@NonNull
	private LocalDate departureDate;

	private List<CarrierPrice> carrierPrices = new ArrayList<>();

	public void addCarrierPrice(CarrierPrice carrierPrice) {
		validateCarrierPrice(carrierPrice);
		carrierPrices.add(carrierPrice);
	}

	private void validateCarrierPrice(CarrierPrice carrierPrice) throws DomainException {
		if (carrierPrice == null) {
			throw new DomainException("Carrier price must not be null");
		}

		if (carrierPrice.getMinPrice() == null || BigDecimal.ZERO.compareTo(carrierPrice.getMinPrice()) >= 0) {
			throw new DomainException("Carrier price must be higher than zero");
		}
	}
}
