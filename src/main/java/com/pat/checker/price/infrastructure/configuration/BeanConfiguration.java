package com.pat.checker.price.infrastructure.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.pat.checker.price.BnkCnkPriceCheckerApplication;
import com.pat.checker.price.domain.service.FlightService;
import com.pat.checker.price.domain.service.FlightServiceImpl;

@Configuration
@ComponentScan(basePackageClasses = BnkCnkPriceCheckerApplication.class)
@PropertySource("classpath:application-${serverRole}.properties")
public class BeanConfiguration {
	@Bean
	FlightService flightService() {
		return new FlightServiceImpl();
	}
}