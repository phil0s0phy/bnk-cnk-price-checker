package com.pat.checker.price.domain;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.junit.Test;

public class FlightTest {

	@Test
	public void shouldAddCarrierPriceSuccessfuly_minPriceIsHigherThanZero() {
		// Given
		String origin = "origin";
		String destination = "destination";
		LocalDate departureDate = LocalDate.now();
		String minPrice = "100.25";
		String carrierName = "Test carrier";

		CarrierPrice carrierPrice = new CarrierPrice(carrierName, new BigDecimal(minPrice));

		// When
		Flight flight = new Flight(origin, destination, departureDate);
		flight.addCarrierPrice(carrierPrice);

		// Then
		assertEquals(origin, flight.getOrigin());
		assertEquals(destination, flight.getDestination());
		assertEquals(departureDate, flight.getDepartureDate());
		assertEquals(departureDate, flight.getDepartureDate());
		assertEquals(carrierName, flight.getCarrierPrices().get(0).getCarrier());
		assertEquals(minPrice, flight.getCarrierPrices().get(0).getMinPrice().toString());
	}

	@Test
	public void shouldThrowDomainException_carrierPriceIsNull() {
		// Given
		String origin = "origin";
		String destination = "destination";
		LocalDate departureDate = LocalDate.now();

		CarrierPrice carrierPrice = null;

		// When
		Flight flight = new Flight(origin, destination, departureDate);

		// Then
		try {
			flight.addCarrierPrice(carrierPrice);
		} catch (DomainException e) {
			assertEquals("Carrier price must not be null", e.getMessage());
		}
	}

	@Test
	public void shouldThrowDomainException_minPriceIsZero() {
		// Given
		String origin = "origin";
		String destination = "destination";
		LocalDate departureDate = LocalDate.now();
		String minPrice = "0.00";
		String carrierName = "Test carrier";

		CarrierPrice carrierPrice = new CarrierPrice(carrierName, new BigDecimal(minPrice));

		// When
		Flight flight = new Flight(origin, destination, departureDate);

		// Then
		try {
			flight.addCarrierPrice(carrierPrice);
		} catch (DomainException e) {
			assertEquals("Carrier price must be higher than zero", e.getMessage());
		}
	}

	@Test
	public void shouldThrowDomainException_minPriceIsLowerThanZero() {
		// Given
		String origin = "origin";
		String destination = "destination";
		LocalDate departureDate = LocalDate.now();
		String minPrice = "-100.00";
		String carrierName = "Test carrier";

		CarrierPrice carrierPrice = new CarrierPrice(carrierName, new BigDecimal(minPrice));

		// When
		Flight flight = new Flight(origin, destination, departureDate);

		// Then
		try {
			flight.addCarrierPrice(carrierPrice);
		} catch (DomainException e) {
			assertEquals("Carrier price must be higher than zero", e.getMessage());
		}
	}

}
