package com.pat.checker.price.domain.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doReturn;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.pat.checker.price.domain.Flight;

@RunWith(PowerMockRunner.class)
@PrepareForTest(FlightServiceImpl.class)
public class FlightServiceImplTest {
	@Mock
	private FlightServiceImpl flightService;

	private LocalDate today = LocalDate.now();

	@Before
	public void setUp() throws IOException {
		doCallRealMethod().when(flightService).getCheapestBnkCnxPrice(any(LocalDate.class));
		doCallRealMethod().when(flightService).extractCarrierPrice(anyString(), anyString(), any(LocalDate.class),
				any(SkyScannerEntity.class), anyMap());
		doCallRealMethod().when(flightService).extractCarrierName(any(SkyScannerEntity.class));
	}

	@Test
	public void shouldProperlyReturnCheapestPrice() throws IOException {
		// Given
		SkyScannerEntity serviceResponse = createResponse();

		// When
		doReturn(serviceResponse).when(flightService).callSkyScannerService(any(LocalDate.class));
		Flight flight = flightService.getCheapestBnkCnxPrice(today);

		// Then
		assertEquals("Bangkok (Suvarnabhumi)", flight.getOrigin());
		assertEquals("Chiang Mai", flight.getDestination());
		assertEquals(today, flight.getDepartureDate());
		assertEquals(1, flight.getCarrierPrices().size());
		assertEquals("Test carrier", flight.getCarrierPrices().get(0).getCarrier());
		assertEquals(new BigDecimal(100), flight.getCarrierPrices().get(0).getMinPrice());
	}

	@Test
	public void shouldReturnUnknownCarrierName_carrierIsEmpty() throws IOException {
		// Given
		SkyScannerEntity serviceResponse = createResponse();
		serviceResponse.getCarriers().clear();

		// When
		doReturn(serviceResponse).when(flightService).callSkyScannerService(any(LocalDate.class));
		Flight flight = flightService.getCheapestBnkCnxPrice(LocalDate.now());

		// Then
		assertEquals("Bangkok (Suvarnabhumi)", flight.getOrigin());
		assertEquals("Chiang Mai", flight.getDestination());
		assertEquals(today, flight.getDepartureDate());
		assertEquals(1, flight.getCarrierPrices().size());
		assertEquals("Unknown", flight.getCarrierPrices().get(0).getCarrier());
		assertEquals(new BigDecimal(100), flight.getCarrierPrices().get(0).getMinPrice());
	}

	@Test
	public void shouldReturnEmptyCarrierPrice_quoteIsEmpty() throws IOException {
		// Given
		SkyScannerEntity serviceResponse = createResponse();
		serviceResponse.getQuotes().clear();

		// When
		doReturn(serviceResponse).when(flightService).callSkyScannerService(any(LocalDate.class));
		Flight flight = flightService.getCheapestBnkCnxPrice(LocalDate.now());

		// Then
		assertEquals("Bangkok (Suvarnabhumi)", flight.getOrigin());
		assertEquals("Chiang Mai", flight.getDestination());
		assertEquals(today, flight.getDepartureDate());
		assertEquals(0, flight.getCarrierPrices().size());
	}

	@Test
	public void shouldReturnUnknownCarrierName_outboundLegIsNull() throws IOException {
		// Given
		SkyScannerEntity serviceResponse = createResponse();
		serviceResponse.getQuotes().get(0).setOutboundLeg(null);

		// When
		doReturn(serviceResponse).when(flightService).callSkyScannerService(any(LocalDate.class));
		Flight flight = flightService.getCheapestBnkCnxPrice(LocalDate.now());

		// Then
		assertEquals("Bangkok (Suvarnabhumi)", flight.getOrigin());
		assertEquals("Chiang Mai", flight.getDestination());
		assertEquals(today, flight.getDepartureDate());
		assertEquals(1, flight.getCarrierPrices().size());
		assertEquals("Unknown", flight.getCarrierPrices().get(0).getCarrier());
		assertEquals(new BigDecimal(100), flight.getCarrierPrices().get(0).getMinPrice());
	}

	@Test
	public void shouldReturnUnknownCarrierName_outboundLegCarrierIdIsEmpty() throws IOException {
		// Given
		SkyScannerEntity serviceResponse = createResponse();
		serviceResponse.getQuotes().get(0).getOutboundLeg().getCarrierIds().clear();

		// When
		doReturn(serviceResponse).when(flightService).callSkyScannerService(any(LocalDate.class));
		Flight flight = flightService.getCheapestBnkCnxPrice(LocalDate.now());

		// Then
		assertEquals("Bangkok (Suvarnabhumi)", flight.getOrigin());
		assertEquals("Chiang Mai", flight.getDestination());
		assertEquals(today, flight.getDepartureDate());
		assertEquals(1, flight.getCarrierPrices().size());
		assertEquals("Unknown", flight.getCarrierPrices().get(0).getCarrier());
		assertEquals(new BigDecimal(100), flight.getCarrierPrices().get(0).getMinPrice());
	}

	private SkyScannerEntity createResponse() {
		OutboundLeg outboundLeg = new OutboundLeg();
		outboundLeg.setCarrierIds(new ArrayList<>());
		outboundLeg.getCarrierIds().add(1);

		Quote quote = new Quote();
		quote.setMinPrice(100);
		quote.setOutboundLeg(outboundLeg);

		List<Quote> quotes = new ArrayList<>();
		quotes.add(quote);

		Carrier carrier = new Carrier();
		carrier.setCarrierId(1);
		carrier.setName("Test carrier");

		List<Carrier> carriers = new ArrayList<>();
		carriers.add(carrier);

		SkyScannerEntity skyScannerEntity = new SkyScannerEntity();
		skyScannerEntity.setQuotes(quotes);
		skyScannerEntity.setCarriers(carriers);

		return skyScannerEntity;
	}
}
